# To push to DockerHub:
#   TAG="$(git log -1 --pretty=%h)" && docker build -t "sinaiiidgst/facets2phylo:$TAG" . && docker push "sinaiiidgst/facets2phylo:$TAG"

FROM continuumio/miniconda:4.7.12

RUN apt-get update && \
    apt-get -y install gcc mono-mcs
RUN apt-get -y install gcc gfortran git texlive
RUN apt-get -y install libgsl0-dev
RUN apt-get -y install g++

RUN conda update --all
RUN conda update -n base conda
RUN conda config --add channels bioconda
RUN conda config --add channels conda-forge

WORKDIR /facets2phylo
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY . ./
RUN g++ -o mh.o -O3 mh.cpp util.cpp `gsl-config --cflags --libs`

WORKDIR /

